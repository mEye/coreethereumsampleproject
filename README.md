## shell
```
truffle migrate --reset
truffle test
truffle console --network development
```

## truffle console
```
account0 = web3.eth.accounts[0]
account1 = web3.eth.accounts[1]

MinimumViableToken.deployed().then(inst => { inst.transfer.call(account1, 100) })
MinimumViableToken.deployed().then(inst => { console.log(inst.balanceOf(account1)) })
```