var MinimumViableToken = artifacts.require("./MinimumViableToken.sol");

module.exports = function(deployer) {
    deployer.deploy(MinimumViableToken, 10000);
};