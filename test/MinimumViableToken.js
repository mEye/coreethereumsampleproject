var MinimumViableToken = artifacts.require("MinimumViableToken")

contract('MinimumViableToken', function (accounts) {
    it("should put 10000 Token in the first account", function() {
        return MinimumViableToken.deployed().then(function(instance) {
            return instance.balanceOf.call(accounts[0]);
        }).then(function(balance) {
            assert.equal(balance.valueOf(), 10000, "10000 wasn't in the first account");
        });
    });

    it("should send coin correctly", function() {
        var MVTInstance;
    
        // Get initial balances of first and second account.
        var account_one = accounts[0];
        var account_two = accounts[1];
    
        var account_one_starting_balance;
        var account_two_starting_balance;
        var account_one_ending_balance;
        var account_two_ending_balance;
    
        var amount = 10;
    
        return MinimumViableToken.deployed().then(function(instance) {
            MVTInstance = instance;
            return MVTInstance.balanceOf.call(account_one);
        }).then(function(balance) {
            account_one_starting_balance = balance.toNumber();
            return MVTInstance.balanceOf.call(account_two);
        }).then(function(balance) {
            account_two_starting_balance = balance.toNumber();
            return MVTInstance.transfer(account_two, amount);
        }).then(function() {
            return MVTInstance.balanceOf.call(account_one);
        }).then(function(balance) {
            account_one_ending_balance = balance.toNumber();
            return MVTInstance.balanceOf.call(account_two);
        }).then(function(balance) {
            account_two_ending_balance = balance.toNumber();
    
            assert.equal(account_one_ending_balance, account_one_starting_balance - amount, "Amount wasn't correctly taken from the sender");
            assert.equal(account_two_ending_balance, account_two_starting_balance + amount, "Amount wasn't correctly sent to the receiver");
        });
      });
});